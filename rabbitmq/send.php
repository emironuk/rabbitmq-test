<?php
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
class rabbit {
function __construct($host="127.0.0.1"){
        $this->connection = new AMQPStreamConnection($host, 5672, 'guest', 'guest');
        $this->channel = $this->connection->channel();
       $this->channel->exchange_declare('test', 'fanout', false,false,false);
#        $this->channel->queue_declare("test", false, false, false, false);
}
function __destruct(){
        $this->channel->close();
        $this->connection->close();
}
public function send($key,$value){
        $msg = new AMQPMessage($value);
        return $this->channel->basic_publish($msg, $key);
}

}

